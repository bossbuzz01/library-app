<?php
require "fpdf.php";

$servidor="localhost";
$usuario="root";
$password="";
$db_name="libreria";

$connection=mysqli_connect($servidor,$usuario,$password,$db_name);

class myPDF extends FPDF{
    
    function header(){
        $this->SetFont('Arial','B',14);
        $this->Cell(0,5,'Reporte de Reposiciones/Donaciones',0,0,'C');
        $this->Ln(20);
    }
    
    function footer(){
        $this->SetY(-15);
        $this->SetFont('Arial','',14);
        $this->Cell(500,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
    }
    
    function headerTable(){
        $this->SetFont('Arial','B',12);
        $this->Cell(25,6,'ID',1,0,'C');
        $this->Cell(60,6,'Titulo',1,0,'C');
        $this->Cell(50,6,'Usuario',1,0,'C');
        $this->Cell(40,6,'Fecha',1,0,'C');
        $this->Cell(45,6,'Tipo',1,0,'C');
        $this->Ln();
    }
    
    function viewTable($connection){
        $this->SetFont('Arial','',11);
        session_start();
        $query = $_SESSION["don_res"];
        $result=mysqli_query($connection,$query);
        while($rows=mysqli_fetch_assoc($result)){
            $titleid = $rows["id_libro"];
            $userid = $rows["id_usuario"];
            $title_res = mysqli_query($connection,"SELECT * FROM libro WHERE id = '$titleid'");
            $user_res = mysqli_query ($connection,"SELECT * FROM usuario WHERE id = '$userid'");
            $title_rows = mysqli_fetch_assoc($title_res);
            $user_rows = mysqli_fetch_assoc($user_res);
            $this->Cell(25,10,$rows["id"],1,0,'L');
            $this->Cell(60,10,$title_rows["titulo"],1,0,'L');
            $this->Cell(50,10,$user_rows["usuario"],1,0,'L');
            $this->Cell(40,10,$rows["fecha"],1,0,'L');
            $this->Cell(45,10,$rows["tipo"],1,0,'L');
            $this->Ln();
        }
    }
}

$pdf=new myPDF();
$pdf->AliasNbPages();
$pdf->AddPage('L','A4',0);
$pdf->headerTable();
$pdf->viewTable($connection);
$pdf->Output();

?>